#!/usr/bin/env python3

import csv

def readCSV(file):
    with open(file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        csvdata = list(spamreader)
        csvdata.pop(0)
        return csvdata

def mois(file):
    mois = file[0][1]

    if mois == '01':
        print('janvier')
        tailles_colonnes = [2,3,2]

    if mois == '02':
        print('février')
        tailles_colonnes = [2,3,3]

    if mois == '03':
        print('maars')
        tailles_colonnes = [1,2,2]

    if mois == '04':
        print('avril')
        tailles_colonnes = [2,3]

    if mois == '05':
        print('mai')
        tailles_colonnes = [1,2]

    if mois == '06':
        print('juin')
        tailles_colonnes = [1,1,2]

    if mois == '07':
        print('juillet')
        tailles_colonnes = [1,1,2]

    if mois == '08':
        print('augustus')
        tailles_colonnes = [2,3,3]

    if mois == '09':
        print('septembre')
        tailles_colonnes = [3,3,3]

    if mois == '10':
        print('octobre')
        tailles_colonnes = [2,3,2]

    if mois == '11':
        print('novembre')
        tailles_colonnes = [3,2,3]

    if mois == '12':
        print('décembre')
        tailles_colonnes = [3,2,3]

    alors = []
    base = 0

    for i, span in enumerate(tailles_colonnes):
        # print(col)
        if i > 0:
            depart = base + tailles_colonnes[i-1]
            base = depart
        else:
            depart = 0
        alors.append((depart, span))

    return alors

file = readCSV('infos.csv')

print(mois(file))

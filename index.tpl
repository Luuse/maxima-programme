<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Maxima Programme {{ mois[0] }} {{ info[1] }}</title>

  <style>
    :root {
      --nb-colonnes-bas: {{ len_widz[0] }};
      --nb-colonnes-bas-bas: {{ len_widz[1] }};
		}

		#infos-maxima {
		  grid-column: 1/calc(var(--nb-colonnes-bas-bas) * 0.5 + 1);
			grid-row: 2/2;
		}

		#habitantes {
		  /* grid-column: calc(var(--nb-colonnes-bas-bas) * 0.5 + 1)/calc(var(--nb-colonnes-bas-bas) * 0.5 + 3); */
		  grid-column: calc(var(--nb-colonnes-bas-bas) * 0.5 + 1)/ -1;
			grid-row: 2/2;
		}

    #haut {
      grid-template-columns: {{ widths[2] }};
    }

    #bas {
      grid-template-columns: {{ widths[0] }};
    }

    #bas-bas {
      grid-template-columns: {{ widths[1] }};
    }

    % for i, col in enumerate(colonz):
      #col-{{ i+4 }} {
        grid-column: {{ col[0] }} / span {{ col[1] }};
      }
    % end

  </style>

  <link rel="stylesheet" type="text/css" media="all" href="static/css/main.css">

</head>
<body class="print">
<main class="page">

  <section id="haut">
    <div class="infos-fixes" id="adresse">{{ info[2] }}</div>
    <div class="infos-fixes" id="site">{{ info[10] }}</div>
		<div class="infos-fixes" id="annee">{{ info[1] }}</div>

      <div class="col" id="col-haut-1"></div>
      <div class="col" id="col-haut-2"></div>
      <div class="col" id="col-haut-3"></div>
  </section>

   % for event in evenements:
        <article>
          <div class="infos-pratiques">
            <p class="lieu">{{ event[3] }}</p>
            <div class="date">
              {{ event[1] }}/{{ info[0] }}
            </div>
            <p class="heure">{{ event[2] }}</p>
          </div>

          <div class="text">
            <div class="titre">
              <p class="type">{{ event[7] }}</p>
              <p class="eventTitle">{{ event[0] }}</p>
              <p class="description">{{ event[5] }} <span class="price">{{ event[6] }}</span></p>
            </div>

            <div class="description-orga">
              <p class="infos">organisé par {{ event[4] }}</p>
              <p class="infos">contact : {{ event[8] }}</p>
            </div>
          </div>
        </article>

    % end

  <section id="bas">
    % for i, col in enumerate(colonz):
      <div class="col" id="col-{{ i+4 }}"></div>
    % end

    <div id="bas-bas">
      <div class="infos-fixes" id="infos-maxima">
        <p>{{ info[7] }}</p>
        <p class="ar">{{ info[8] }}</p>
        <p>{{ info[9] }}</p>

      </div>

      <div class="infos-fixes" id="habitantes">
        <p>{{ info[3] }}</p>
        <p class="ar">{{ info[4] }}</p>
        <p>{{ info[5] }}</p>  
        <p class="mail">{{ info[6] }}</p>
        <div class="" id="soutien">{{ info[11] }}</div>
      </div>

    </div>
  </section>

</main>
  <p id="mois1" class="hidden">{{ mois[0] }}</p>
  <p id="mois2" class="hidden">{{ mois[1] }}</p>

  <p id="widths1" class="hidden">{{ widths[0] }}</p>
  <p id="widths2" class="hidden">{{ widths[1] }}</p>
  <p id="widths3" class="hidden">{{ widths[2] }}</p>

  % for col in colonz:
    <p class="hidden colonz">{{ col }}</p>
  % end

 <script src="static/js/maxi.js"></script>
</body>
</html>

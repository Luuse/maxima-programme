# à faire
- [ ] test font / lisibilité de l'arabe (Camille doit nous envoyer le numéro d'Hamid)
- [ ] faire un guide d'utilisation sous forme de site
    - [ ] guidelines + liens vers les tableurs + date limite pour remplir
    - [ ] visualisation le PDF en direct
    - [ ] une ligne d'exemple formatage dans le tableur
- [ ] deux couches pour lisibilité ? : une précouche orange toujours pareille (imprimer un seul master pour tous les mois), + une couche noire. à voir avec Meriem.
- [ ] rajouter une case pour la photo : récup à partir d'un URL du drive.
- [ ] style de dates pour certains évènements "multidates", par ex : ateliers sur plusieurs jours, festivals.
- [ ] indiquer que dans les cas particuliers : mettre en description (ex : horaires différentes, etc)

# notes rdv avec Camille :
- pliage et découpage : OK
- nombre d'évènements : ex en juillet 2021 une dizaine.
- chaîne évènement Maxima sur Slack
- Communa cherche un stagiaire pour faire la com

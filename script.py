#!/usr/bin/env python3

import csv
import bottle
import os
import requests
import subprocess
import re

import scale

# FONCTIONS


def extractCSV(file_url, file):
    r = requests.get(file_url, stream=True)

    with open(file, "wb") as csv_file:
        for chunk in r.iter_content(chunk_size=1024):
            # writing one chunk at a time to pdf file
            if chunk:
                csv_file.write(chunk)


def readCSV(file):
    with open(file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=",")
        csvdata = list(spamreader)
        csvdata.pop(0)
        return csvdata


def get_chars_width(mot):
    widths = []
    sumWidz = 0
    with open("svg-generator/mp/glyphs.mp", "r", encoding="utf-8") as f:
        gl_meta = f.read()
        for char in mot.upper():
            ordchar = str(ord(char))
            cherche = re.search(r"beginchar\(%s,(.*)\)" % ordchar, gl_meta)
            charwidth = cherche.groups()[0]

            sumWidz += int(charwidth)
            widths.append(charwidth)

    pourcentages = ""
    for w in widths:
        prct = round(int(w) / sumWidz * 100, 2)
        pourcentages += str(prct) + "% "

    return pourcentages.rstrip(" "), len(widths)


def taille_colonnes(file):
    mois = int(file[0])

    months, tailles_colonnes = [[], []]

    if mois == 1:
        months = ["janvier", "januari"]
        tailles_colonnes = [2, 3, 2]

    if mois == 2:
        months = ["februari", "février"]
        tailles_colonnes = [2, 3, 3]

    if mois == 3:
        months = ["maart", "mars"]
        tailles_colonnes = [1, 2, 2]

    if mois == 4:
        months = ["avril", "april"]
        tailles_colonnes = [2, 3]

    if mois == 5:
        months = ["mei", "mai"]
        tailles_colonnes = [1, 2]

    if mois == 6:
        months = ["juni", "juin"]
        tailles_colonnes = [1, 1, 2]

    if mois == 7:
        months = ["juillet", "juli"]
        tailles_colonnes = [2, 3, 2]

    if mois == 8:
        months = ["augustus", "août"]
        tailles_colonnes = [3, 2, 3]

    if mois == 9:
        months = ["september", "septembre"]
        tailles_colonnes = [3, 3, 3]

    if mois == 10:
        months = ["october", "octobre"]
        tailles_colonnes = [2, 3, 2]

    if mois == 11:
        months = ["novembre", "november"]
        tailles_colonnes = [3, 2, 3]

    if mois == 12:
        months = ["december", "décembre"]
        tailles_colonnes = [3, 2, 3]

    alors = []
    base = 1

    for i, span in enumerate(tailles_colonnes):
        if i > 0:
            depart = base + tailles_colonnes[i - 1]
            base = depart
        else:
            depart = 1
        alors.append((depart, span))

    return months, alors


# CÉPARTI
@bottle.get("/static/<filepath:path>")
def static(filepath):
    return bottle.static_file(filepath, root="static/")


@bottle.get("/<filepath:path>")
def root(filepath):
    return bottle.static_file(filepath, root=".")


@bottle.route("/")
def index():
    # extractCSV("https://lite.framacalc.org/maxima-programme.csv", "evenement.csv")
    # extractCSV("https://lite.framacalc.org/maxima-infos.csv", "infos.csv")
    evenements = readCSV("evenement.csv")
    infos = readCSV("infos.csv")[0]

    mois, colonz = taille_colonnes(infos)

    widz = [
        get_chars_width(mois[0])[0],
        get_chars_width(mois[1])[0],
        get_chars_width("MAXIMA")[0],
    ]

    len_widz = (get_chars_width(mois[0])[1], get_chars_width(mois[1])[1])

    return bottle.template(
        "index.tpl",
        evenements=evenements,
        info=infos,
        mois=mois,
        widths=widz,
        len_widz=len_widz,
        colonz=colonz,
    )


@bottle.route("/setValue", method="post")
def set_Value():
    mot = bottle.request.forms.mot
    he = bottle.request.forms.height

    print("height: " + he)
    scale.build_mot(mot, he.split(","))


bottle.run(bottle.app(), host="0.0.0.0", port=8082, debug=True, reload=True)

## Installation
2021

`sudo pip install bottle`

## Usage
* calc évènements : https://lite.framacalc.org/maxima-programme
* calc infos : https://lite.framacalc.org/info-maxima

### Générer les glyphes de base
`mpost -interaction=batchmode svg-generator/mp/base.mp`

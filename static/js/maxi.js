///////////////

function getServer(mot, heights, chiff, grille, arrayPos) {
  // Requête AJAX pour parler au serveur
  const heightString = heights.join(","); // conversion de l'array en string

  let xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      moMot(mot, chiff, grille, arrayPos);
      console.log("AJAX", mot);
    }
  };

  xmlhttp.open("POST", "/setValue", true);
  xmlhttp.send("mot=" + mot + "&height=" + heightString);
}

function prepend(value, array) {
  let newArray = array.slice();
  newArray.unshift(value);
  return newArray;
}

function moMot(mot, chiff, grille, arrayPos) {
  let baseWidz = 5;
  let largeur = 0;

  const widths = document.getElementById("widths" + chiff).innerHTML;
  const widz = widths.replace(/%/g, "").split(" ");

  mot.split("").forEach(function (letter, i) {
    let image = document.createElement("img");
    image.src = "svg-generator/svg/" + mot + "/" + i + ".svg";
    image.position = "absolute";

    image.style.left = baseWidz + "mm";

    if (chiff == 3) {
      image.style.top = (arrayPos[i] + 37.795275591).toString() + "px";
    } else if (chiff == 1) {
      image.style.bottom = hauteurBas;
    } else if (chiff == 2) {
      // la hauteur de page totale fait 42cm donc 1587.4015748px
      image.style.top = 1587.4015748 - parseInt(hauteurBas);
    }

    largeur = (widz[i] * 287) / 100;

    baseWidz += largeur;

    grille.append(image);
  });
}

///////////////////////
//////  CÉPARTI  //////
///////////////////////

// Pour être sûr·e que tout est bien chargé avant de calculer
// les positions et les tailles des éléments
window.onload = function () {
  ///////////////
  // VARIABLES //
  ///////////////

  // Placement lettres
  const haut = document.getElementById("haut");
  const bas = document.getElementById("bas");
  const basBas = document.getElementById("bas-bas");

  const wrappersHaut = document.querySelectorAll("#haut .col");
  const wrappersBas = document.querySelectorAll("#bas .col");

  const mois1 = document.getElementById("mois1").innerHTML.toUpperCase();
  const mois2 = document.getElementById("mois2").innerHTML.toUpperCase();

  ///////////////
  //  LE CODE  //
  ///////////////

  let year;

  function spaceYear() {
    let content = year.innerHTML.split("").join(" ");
    year.innerHTML = content;
  }

  year = document.getElementById("annee");
  spaceYear();

  let colHaut = document.querySelectorAll("#haut .col");
  let colBas = document.querySelectorAll("#bas .col");
  let events = [...document.getElementsByTagName("article")];

  if (events.length < 24) {
    let nbArticlesToAddTop = Math.round((24 - events.length) / 2);
    let nbArticlesToAddBottom = parseInt((24 - events.length) / 2);

    for (let i = 0; i < nbArticlesToAddTop; i++) {
      let article = document.createElement("article");
      article.classList.add("hidden");
      haut.append(article);
    }
    for (let i = 0; i < nbArticlesToAddBottom; i++) {
      let article = document.createElement("article");
      article.classList.add("hidden");
      bas.append(article);
    }
  }

  events = [...document.getElementsByTagName("article")];
  let limite = Math.ceil(events.length / 2);

  // placement évènements
  events.forEach(function (ev, i) {
    if (i < limite) {
      colHaut[i % colHaut.length].appendChild(ev);
    } else {
      colBas[i % colBas.length].appendChild(ev);
    }
  });

  // Défini pas trop haut puisque la valeur est modifié par l'ajout des éléments
  let baseHauteurBas = parseInt(window.getComputedStyle(basBas).height);
  hauteurBas = (27.795275591 + baseHauteurBas).toString() + "px";
  // hauteurBas = (37.795275591 + baseHauteurBas).toString() + "px";

  // Calculer la place dispo pour chaque lettre du MAXIMA
  // -> la largeur et la hauteur de chaque élément
  let wrapHeight = [];

  wrapHeight = {
    haut: [],
    bas: [],
    basBas: {},
  };

  [...wrappersHaut].forEach(function (el) {
    wrapHeight["haut"].push(window.getComputedStyle(el).height);
    wrapHeight["haut"].push(window.getComputedStyle(el).height);
  });

  const colonz = document.getElementsByClassName("colonz");

  [...wrappersBas].forEach(function (el, i) {
    const count = parseInt(colonz[i].innerHTML[4]);

    for (let j = 0; j < count; j++) {
      wrapHeight["bas"].push(window.getComputedStyle(el).height);
    }
  });

  const wrappersBasBas = document.querySelectorAll("#bas-bas > *");

  [...wrappersBasBas].forEach(function (el, i) {
    const count = wrappersBasBas.length;

    const gridTpl = window.getComputedStyle(el).gridColumn.split(" / ");

    const nbColonnesBasBas = getComputedStyle(document.body).getPropertyValue(
      "--nb-colonnes-bas-bas"
    );

    const debut = parseInt(gridTpl[0]);

    let fin;

    if (gridTpl[1] == -1) {
      fin = parseInt(nbColonnesBasBas) + 1;
    } else {
      fin = parseInt(gridTpl[1]);
    }

    const heightLa = [];

    for (let o = debut; o < fin; o++) {
      heightLa.push(o);
    }

    mois2.split("").forEach(function (letter, k) {
      if (heightLa.includes(k + 1)) {
        wrapHeight.basBas[k + 1] = window.getComputedStyle(el).height;
      }
    });
  });

  // 20cm = 755.90551181px

  let heightsHaut = [];
  let infosFixesHaut = [35.64, 0, 47.984, 47.984, 47.984, 47.984];
  wrapHeight.haut.forEach(function (hoba, l) {
    const h = parseFloat(hoba);
    heightsHaut.push(755.90551181 - h - infosFixesHaut[l]);
  });

  let heightsBas = [];
  wrapHeight.bas.forEach(function (hoba) {
    const h = parseFloat(hoba);
    heightsBas.push(755.90551181 - h - baseHauteurBas);
  });

  let heightsBasBas = [];

  mois2.split("").forEach(function (letter, l) {
    // if (wrapHeight.basBas[l]) {
    let h = parseFloat(wrapHeight.basBas[l + 1]);
    heightsBasBas.push(baseHauteurBas - h);
    // }
  });

  // // Renvoyer les infos au serveur
  getServer("MAXIMA", heightsHaut, 3, haut, infosFixesHaut);
  getServer(mois1, heightsBas, 1, bas, [0]);
  getServer(mois2, heightsBasBas, 2, basBas, [0]);
};

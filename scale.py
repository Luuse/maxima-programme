import os
import re
import sys
import subprocess
import svgutils as su
import lxml.etree as et
from svg.path import parse_path


def proper_round(num, dec=0):
    num = str(num)[: str(num).index(".") + dec + 2]
    if num[-1] >= "5":
        return float(num[: -2 - (not dec)] + str(int(num[-2 - (not dec)]) + 1))
    return float(num[:-1])


def getInfoPath(f, pattern):
    file = open(f, "r")
    elem = et.parse(file)
    root = elem.getroot()
    for child in root:
        for child in root:
            if child.tag == "{http://www.w3.org/2000/svg}path":
                if child.attrib["style"].startswith(pattern):
                    dparse = parse_path(child.attrib["d"])
                    for point in dparse:
                        type = str(point)[0:4]
                        x = dparse[1].start.real
                        y = dparse[2].end.imag * -1
                        h = dparse[1].start.imag
                        w = dparse[1].end.real
    file.close()
    return [x, y, w, h]


def exeMP(fichier):
    Command = "mpost -interaction=batchmode svg-generator/mp/%s.mp" % fichier
    process = subprocess.Popen(Command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()


def changeWidth(x):
    file = open("svg-generator/mp/unity.mp", "w")
    file.write("ux= %s * u;" % x)
    file.write("uy= 1 * u;")
    file.close()


def calcul_largeur(mot):
    # On réinitialise la valeur de x
    changeWidth(1)

    x = 0
    for c in mot:
        letter = ord(c)
        svg = "svg-generator/svg/%s.svg" % letter
        info = getInfoPath(svg, "stroke:rgb(100.000000%,0.000000%,0.000000%);")
        x = x + info[2]

    # La largeur est toujours celle de la page moins les marges, donc 287mm
    # Ça fait environ 1084.7 pixels (1084.7240944 mais là ça pinaille)
    coefX = proper_round(1084.7240944 / x, 2)

    # On calcule la nouvelle valeur
    changeWidth(coefX)


def meta_file(mot, heights):
    # Génère un dossier "mot" contenant les svgs à la bonne taille
    with open("svg-generator/mp/base.mp", "r", encoding="utf-8") as b:
        base = b.read()
    inclusion = ""

    if not os.path.isdir("svg-generator/svg/{0}".format(mot)):
        os.mkdir("svg-generator/svg/{0}".format(mot))

    # Générer le fichier mp pour le mot correspondant
    for i, char in enumerate(mot):
        coefY = float(heights[i]) / 438.35510

        print(char, end="-")
        with open("svg-generator/mp/glyphs/%s.mp" % ord(char)) as tmp:
            inclusion += tmp.read()
            inclusion = inclusion.replace("beginchar(%s" % str(ord(char)), "beginchar(%s" % str(i))
            inclusion = re.sub(r"(beginchar\(%s.*)\)" % str(i), r"\1,%s)" % coefY, inclusion)

    print("\n")

    combi = base.replace("input glyphs", inclusion)
    remplace = 'outputtemplate := "svg-generator/svg/{0}/%c.svg"'.format(mot)

    combi = combi.replace(
        'outputtemplate := "svg-generator/svg/%c.svg"', remplace)

    combi += "end;"

    with open("svg-generator/mp/combi.mp", "w", encoding="utf-8") as f:
        f.write(combi)


def build_mot(mot, heights):
    # On exécute tout
    calcul_largeur(mot)
    meta_file(mot, heights)
    exeMP("combi")
